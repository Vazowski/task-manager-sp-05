package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.dto.ProjectDTO;

public interface IProjectRepository extends IAbstractRepository<ProjectDTO>{

    void update(@NotNull final ProjectDTO projectDTO);

    ProjectDTO findByName(@NotNull final String name);
}
