package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.dto.TaskDTO;

public interface ITaskRepository extends IAbstractRepository<TaskDTO>{

    void update(@NotNull final TaskDTO taskDTO);

    TaskDTO findByName(@NotNull final String name);
}
