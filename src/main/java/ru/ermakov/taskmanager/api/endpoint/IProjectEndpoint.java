package ru.ermakov.taskmanager.api.endpoint;

import ru.ermakov.taskmanager.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void updateProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws Exception;

    @WebMethod
    ProjectDTO findById(
            @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    ProjectDTO findByName(
            @WebParam(name = "projectName") String projectName
    ) throws Exception;

    @WebMethod
    List<ProjectDTO> findAllProject() throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProject() throws Exception;
}
