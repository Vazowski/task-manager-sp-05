package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.dto.TaskDTO;

import javax.faces.bean.ManagedBean;
import java.util.List;

@Component
@ManagedBean(eager = true)
public class TaskController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @Nullable
    private TaskDTO currentTask = new TaskDTO();

    public void onload(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            return;
        }
        currentTask = taskService.findById(id);
    }

    @Nullable
    public TaskDTO getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(@Nullable final TaskDTO currentTask) {
        this.currentTask = currentTask;
    }

    @Nullable
    public TaskDTO task(@Nullable final String id) {
        if (id == null || id.isEmpty())
            return null;
        return taskService.findById(id);
    }

    @Nullable
    public List<TaskDTO> list() {
        return taskService.findAll();
    }

    @Nullable
    public String save(@Nullable final TaskDTO task) {
        taskService.update(task);
        currentTask = null;
        return "pretty:taskList";
    }

    public void delete(@Nullable final TaskDTO task) {
        if (task == null)
            return;
        taskService.remove(task.getId());
    }

    public void clear() {
        taskService.removeAll();
    }

    public ReadinessStatus[] getReadinessStatuses() {
        return ReadinessStatus.values();
    }
}
