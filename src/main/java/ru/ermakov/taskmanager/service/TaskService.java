package ru.ermakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.dto.TaskDTO;
import ru.ermakov.taskmanager.repository.TaskRepository;
import ru.ermakov.taskmanager.util.TaskDTOConvertUtil;

import java.util.List;

@Service
@Transactional
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    public TaskService() {
    }

    public void update(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null)
            return;
        taskRepository.save(TaskDTOConvertUtil.DTOToTask(taskDTO));
    }

    @Nullable
    public TaskDTO findById(@Nullable final String id) {
        if (id.isEmpty())
            return null;
        return TaskDTOConvertUtil.taskToDTO(taskRepository.getOne(id));
    }

    @Nullable
    public TaskDTO findByName(@Nullable final String name) {
        if (name.isEmpty())
            return null;
        return TaskDTOConvertUtil.taskToDTO(taskRepository.findByName(name));
    }

    @Nullable
    public List<TaskDTO> findAll() {
        return TaskDTOConvertUtil.tasksToDTO(taskRepository.findAll());
    }

    public void remove(@NotNull final String id) {
        if (id.isEmpty())
            return;
        taskRepository.delete(taskRepository.getOne(id));
    }

    public void removeAll() {
        taskRepository.deleteAll();
    }
}