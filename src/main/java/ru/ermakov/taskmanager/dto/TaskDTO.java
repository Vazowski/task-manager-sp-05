package ru.ermakov.taskmanager.dto;

public class TaskDTO extends AbstractDealDTO {

    private String projectId;

    public TaskDTO() {
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "ID: " + this.getId() +
                "\nprojectId: " + this.projectId +
                "\nName: " + this.getName() +
                "\nDescription: " + this.getDescription() +
                "\nDateCreated: "+ this.getDateCreated() +
                "\nDateBegin: " + this.getDateBegin() +
                "\nDateEnd: " + this.getDateEnd() +
                "\nStatus: " + this.getReadinessStatus();
    }
}
