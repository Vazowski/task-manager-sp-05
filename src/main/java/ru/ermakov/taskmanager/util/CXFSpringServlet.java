package ru.ermakov.taskmanager.util;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.support.XmlWebApplicationContext;

import javax.servlet.ServletConfig;
import javax.xml.ws.Endpoint;
import java.lang.reflect.Field;
import java.util.Collection;

public class CXFSpringServlet extends CXFServlet {

    @Override
    protected void loadBus(@Nullable final ServletConfig servletConfig) {

        super.loadBus(servletConfig);

        @Nullable
        final XmlWebApplicationContext ctx = getContext();
        @NotNull
        final AutowireCapableBeanFactory beanFactory = ctx.getParent().getAutowireCapableBeanFactory();
        @Nullable
        final Collection<Endpoint> endpoints = ctx.getBeansOfType(Endpoint.class).values();

        for(@Nullable final Endpoint endpoint : endpoints) {
            beanFactory.autowireBean(endpoint.getImplementor());
        }
    }

    @Nullable
    private XmlWebApplicationContext getContext() {
        try {
            @Nullable
            final Field field = CXFServlet.class.getDeclaredField("createdContext");
            field.setAccessible(true);
            return (XmlWebApplicationContext) field.get(this);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException("Unable to autowire endpoint impementors registered by CXFServlet.", e);
        }
    }
}
